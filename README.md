# README #

Esports statistics data provider library

# Usage

```php
require '../../vendor/autoload.php';
$provider = new \EsportStats\Provider\HelisProvider(new \GuzzleHttp\Client(), 'http://join.helis.lt/api', '');
$esportStats = new \EsportStats\EsportStats($provider);
$teams = $esportStats->getTeams(1, 10);
```