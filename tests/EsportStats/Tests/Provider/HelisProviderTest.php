<?php

namespace EsportStats\Tests\Provider;

use EsportStats\Model\Player;
use EsportStats\Provider\HelisProvider;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class HelisProviderTest
 * @package EsportStats\Tests\Provider
 */
class HelisProviderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public static function tournamentsProvider()
    {
        return [[file_get_contents(__DIR__.'/tournaments.json')]];
    }

    /**
     * @return array
     */
    public static function teamsProvider()
    {
        return [[file_get_contents(__DIR__.'/teams.json')]];
    }

    /**
     * @return array
     */
    public static function tournamentStatisticsProvider()
    {
        return [[file_get_contents(__DIR__.'/tournamentStatistics.json')]];
    }

    /**
     * @dataProvider tournamentsProvider
     * @param string $data
     */
    public function testGetTournamentList($data)
    {
        $helisProvider = $this->getHelisProvider($data);

        $tournaments = $helisProvider->getTournamentList(1, 10);
        $this->assertCount(2, $tournaments);
        $tournament = reset($tournaments);
        $this->assertEquals(19, $tournament->getId());
        $this->assertEquals("Von-Leuschke Championship", $tournament->getName());
        $this->assertEquals(new \DateTime("2017-04-28T21:11:17+0000"), $tournament->getStartsAt());
        $this->assertEquals(new \DateTime("2017-05-20T21:11:17+0000"), $tournament->getEndsAt());
        $this->assertCount(2, $tournament->getTeams());
    }

    /**
     * @param string $data
     * @return HelisProvider
     */
    private function getHelisProvider($data)
    {
        $clientMock = $this->getClientMock($this->getResponseMock($data));
        $helisProvider = new HelisProvider($clientMock, 'http://helis.lt/api', '');

        return $helisProvider;
    }

    /**
     * @param \PHPUnit_Framework_MockObject_MockObject $responseMock
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getClientMock($responseMock)
    {
        $clientMock = $this->createMock(ClientInterface::class);
        $clientMock
            ->expects($this->once())
            ->method('request')
            ->willReturn($responseMock);

        return $clientMock;
    }

    /**
     * @param string $data
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getResponseMock($data)
    {
        $responseMock = $this->createMock(ResponseInterface::class);
        $responseMock
            ->expects($this->once())
            ->method('getBody')
            ->willReturn($data);

        return $responseMock;
    }

    /**
     * @dataProvider teamsProvider
     * @param string $data
     */
    public function testGetTeamList($data)
    {
        $helisProvider = $this->getHelisProvider($data);

        $teams = $helisProvider->getTeamList(1, 10);
        $this->assertCount(2, $teams);
        $team = reset($teams);
        $this->assertEquals(1, $team->getId());
        $this->assertEquals("Peru Feil, Bergnaum and Lowe", $team->getName());
        $this->assertEquals(new \DateTime("2016-10-29T20:19:49+0000"), $team->getCreatedAt());
        $this->assertCount(2, $team->getPlayers());
        $this->assertCount(2, $team->getTournaments());
    }

    /**
     * @dataProvider tournamentStatisticsProvider
     * @param string $data
     */
    public function testGetTournamentStatistic($data)
    {
        $helisProvider = $this->getHelisProvider($data);

        $stats = $helisProvider->getTournamentStatistic(1, 1, 10);
        $this->assertCount(2, $stats);
        $stat = reset($stats);
        $this->assertEquals(8, $stat->getId());
        $this->assertEquals(16, $stat->getKills());
        $this->assertEquals(19, $stat->getDeaths());
        $this->assertInstanceOf(Player::class, $stat->getPlayer());
    }
}