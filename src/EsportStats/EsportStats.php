<?php

namespace EsportStats;

use EsportStats\Model\Team;
use EsportStats\Model\Tournament;
use EsportStats\Model\TournamentStatistic;
use EsportStats\Provider\Provider;

/**
 * Class EsportStats
 * @package EsportStats
 */
class EsportStats
{
    /**
     * @var Provider
     */
    private $provider;

    /**
     * EsportStats constructor.
     * @param Provider $provider
     */
    public function __construct(Provider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param int $page
     * @param int $limit
     * @return Tournament[]
     */
    public function getTournaments($page, $limit)
    {
        return $this->provider->getTournamentList($page, $limit);
    }

    /**
     * @param int $tournamentId
     * @param int $page
     * @param int $limit
     * @return TournamentStatistic
     */
    public function getTournamentStatistics($tournamentId, $page, $limit)
    {
        return $this->provider->getTournamentStatistic($tournamentId, $page, $limit);
    }

    /**
     * @param int $page
     * @param int $limit
     * @return Team[]
     */
    public function getTeams($page, $limit)
    {
        return $this->provider->getTeamList($page, $limit);
    }
}