<?php

namespace EsportStats\Model;

/**
 * Class TournamentStatistic
 * @package EsportStats\Model
 */
class TournamentStatistic
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $kills;
    /**
     * @var int
     */
    private $deaths;
    /**
     * @var Player
     */
    private $player;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return TournamentStatistic
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getKills()
    {
        return $this->kills;
    }

    /**
     * @param int $kills
     * @return TournamentStatistic
     */
    public function setKills($kills)
    {
        $this->kills = $kills;

        return $this;
    }

    /**
     * @return int
     */
    public function getDeaths()
    {
        return $this->deaths;
    }

    /**
     * @param int $deaths
     * @return TournamentStatistic
     */
    public function setDeaths($deaths)
    {
        $this->deaths = $deaths;

        return $this;
    }

    /**
     * @return Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @param Player $player
     * @return TournamentStatistic
     */
    public function setPlayer($player)
    {
        $this->player = $player;

        return $this;
    }
}