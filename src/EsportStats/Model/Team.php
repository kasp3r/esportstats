<?php

namespace EsportStats\Model;

/**
 * Class Team
 * @package EsportStats\Model
 */
class Team
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var \DateTime
     */
    private $createdAt;
    /**
     * @var Player[]
     */
    private $players;
    /**
     * @var Tournament[]
     */
    private $tournaments;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Team
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Team
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Player[]
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @param Player[] $players
     * @return Team
     */
    public function setPlayers($players)
    {
        $this->players = $players;

        return $this;
    }

    /**
     * @param Player $player
     * @return Team
     */
    public function addPlayer(Player $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * @return Tournament[]
     */
    public function getTournaments()
    {
        return $this->tournaments;
    }

    /**
     * @param Tournament[] $tournaments
     * @return Team
     */
    public function setTournaments($tournaments)
    {
        $this->tournaments = $tournaments;

        return $this;
    }

    /**
     * @param Tournament $tournament
     * @return Team
     */
    public function addTournament(Tournament $tournament)
    {
        $this->tournaments[] = $tournament;

        return $this;
    }
}