<?php

namespace EsportStats\Provider;

use EsportStats\Model\Player;
use EsportStats\Model\Team;
use EsportStats\Model\Tournament;
use EsportStats\Model\TournamentStatistic;
use GuzzleHttp\ClientInterface;

/**
 * Class HelisProvider
 * @package EsportStats\Provider
 */
class HelisProvider implements Provider
{
    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var string
     */
    private $apiEndpoint;
    /**
     * No usage found, so it is only a placeholder for a task
     * @var string
     */
    private $apiCredentials;

    /**
     * HelisProvider constructor.
     * @param ClientInterface $client
     * @param string $apiEndpoint
     * @param string $apiCredentials
     */
    public function __construct(ClientInterface $client, $apiEndpoint, $apiCredentials)
    {
        $this->client = $client;
        $this->apiEndpoint = $apiEndpoint;
        $this->apiCredentials = $apiCredentials;
    }

    /**
     * @param int $page
     * @param int $limit
     * @return Tournament[]
     */
    public function getTournamentList($page, $limit)
    {
        $requestUrl = sprintf('%s/tournaments?limit=%d&page=%d', $this->apiEndpoint, $limit, $page);
        $body = $this->sendApiRequest($requestUrl);

        $tournaments = [];
        foreach ($body as $tournamentItem) {
            $tournament = $this->mapTournament($tournamentItem);
            foreach ($tournamentItem['teams'] as $teamItem) {
                $team = $this->mapTeam($teamItem);
                $tournament->addTeam($team);
            }

            $tournaments[] = $tournament;
        }

        return $tournaments;
    }

    /**
     * @param string $requestUrl
     * @return array
     */
    private function sendApiRequest($requestUrl)
    {
        $response = $this->client->request('GET', $requestUrl);
        $body = $response->getBody();
        $bodyDecoded = json_decode($body, true);

        return $bodyDecoded;
    }

    /**
     * @param array $tournamentItem
     * @return Tournament
     */
    private function mapTournament($tournamentItem)
    {
        return (new Tournament())
            ->setId($tournamentItem['id'])
            ->setName($tournamentItem['name'])
            ->setStartsAt(new \DateTime($tournamentItem['starts_at']))
            ->setEndsAt(new \DateTime($tournamentItem['ends_at']));
    }

    /**
     * @param array $teamItem
     * @return Team
     */
    private function mapTeam($teamItem)
    {
        return (new Team())
            ->setId($teamItem['id'])
            ->setName($teamItem['name'])
            ->setCreatedAt(new \DateTime($teamItem['created_at']));
    }

    /**
     * @param int $page
     * @param int $limit
     * @return Team[]
     */
    public function getTeamList($page, $limit)
    {
        $requestUrl = sprintf('%s/teams?limit=%d&page=%d', $this->apiEndpoint, $limit, $page);
        $body = $this->sendApiRequest($requestUrl);

        $teams = [];
        foreach ($body as $teamItem) {
            $team = $this->mapTeam($teamItem);
            foreach ($teamItem['players'] as $playerItem) {
                $player = $this->mapPlayer($playerItem);
                $team->addPlayer($player);
            }
            foreach ($teamItem['tournaments'] as $tournamentItem) {
                $tournament = $this->mapTournament($tournamentItem);
                $team->addTournament($tournament);
            }

            $teams[] = $team;
        }

        return $teams;
    }

    /**
     * @param array $playerItem
     * @return Player
     */
    private function mapPlayer($playerItem)
    {
        return (new Player())
            ->setId($playerItem['id'])
            ->setName($playerItem['name'])
            ->setCreatedAt(new \DateTime($playerItem['created_at']));
    }

    /**
     * @param int $tournamentId
     * @param int $page
     * @param int $limit
     * @return TournamentStatistic[]
     */
    public function getTournamentStatistic($tournamentId, $page, $limit)
    {
        $requestUrl = sprintf(
            '%s/tournament/%d/statistics?limit=%d&page=%d',
            $this->apiEndpoint,
            $tournamentId,
            $limit,
            $page
        );
        $body = $this->sendApiRequest($requestUrl);

        $tournamentStatistics = [];
        foreach ($body as $statisticItem) {
            $statistic = $this->mapTournamentStatistic($statisticItem);
            $player = $this->mapPlayer($statisticItem['player']);
            $statistic->setPlayer($player);
            $tournamentStatistics[] = $statistic;
        }

        return $tournamentStatistics;
    }

    /**
     * @param array $statisticItem
     * @return TournamentStatistic
     */
    private function mapTournamentStatistic($statisticItem)
    {
        return (new TournamentStatistic())
            ->setId($statisticItem['id'])
            ->setKills($statisticItem['kills'])
            ->setDeaths($statisticItem['deaths']);
    }
}