<?php

namespace EsportStats\Provider;

use EsportStats\Model\Team;
use EsportStats\Model\Tournament;
use EsportStats\Model\TournamentStatistic;

/**
 * Interface Provider
 * @package EsportStats\Provider
 */
interface Provider
{
    /**
     * @param int $page
     * @param int $limit
     * @return Tournament[]
     */
    public function getTournamentList($page, $limit);

    /**
     * @param int $page
     * @param int $limit
     * @return Team[]
     */
    public function getTeamList($page, $limit);

    /**
     * @param $tournamentId
     * @param $page
     * @param $limit
     * @return TournamentStatistic
     */
    public function getTournamentStatistic($tournamentId, $page, $limit);
}